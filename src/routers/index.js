import VueRouter from 'vue-router'

import AnimationBasic from '../animation-pages/1.Basic/index'
import AnimationBasicDetail from '../animation-pages/1.Basic/DetailView'
import AnimationOptimized from '../animation-pages/2.Optimized/index'
import AnimationOptimizedDetail from '../animation-pages/2.Optimized/DetailView'
import AnimationPerfect from '../animation-pages/3.Perfect/index'
import AnimationPerfectDetail from '../animation-pages/3.Perfect/DetailView'
import AnimationSample from '../animation-pages/AnimationSample/index'

import AnimationFinal from '../animation-pages/Final'

const routes = [{
    path: '/1',
    component: AnimationBasic,
    name: 'AnimationBasic'
  },
  {
    path: '/1/detail',
    component: AnimationBasicDetail,
    name: 'AnimationBasicDetail'
  },
  {
    path: '/2',
    component: AnimationOptimized,
    name: 'AnimationOptimized',
    children: [{
      path: '4',
      component: AnimationOptimizedDetail,
      name: 'AnimationOptimizedDetail'
    }]
  },
  {
    path: '/3',
    component: AnimationPerfect,
    name: 'AnimationPerfect',
    children: [{
      path: 'detail',
      component: AnimationPerfectDetail,
      name: 'AnimationPerfectDetail'
    }]
  },
  {
    path: '/4',
    component: AnimationFinal,
    name: 'AnimationFinal'
  },
  {
    path: '/5',
    component: AnimationSample,
    name: 'AnimationSample'
  },
  {
    path: '*',
    redirect: {
      name: 'AnimationBasic'
    }
  }
]

export default new VueRouter({
  routes // (缩写) 相当于 routes: routes
})