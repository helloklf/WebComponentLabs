import Vue from 'vue'
import './App.scss'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './routers'
import NavBar from './components/NavBar.vue'
Vue.use(VueRouter)

Vue.component('NavBar', NavBar)

// 4. 创建和挂载根实例。
// 记得要通过 router 配置参数注入路由，
// 从而让整个应用都有路由功能
new Vue({
  el: '#app',
  router,
  render (h) {
    return h(App)
  }
})
// 现在，应用已经启动了！
